package dto;

public interface IOoperaciones {
	
	public double suma(double num1, double num2);
	public double resta(double num1, double num2);
	public double multiplicacion(double num1, double num2);
	public double potencias(double num1, double num2);
	public double raizCuadrada(double num1);
	public double raizCubica(double num1);
	public double division(double num1, double num2) throws ArithmeticException;

}
