package dto;

public class Operaciones implements IOoperaciones {


	@Override
	public double suma(double num1, double num2) {		
		return num1 + num2;
	}

	@Override
	public double resta(double num1, double num2) {		
		return num1 - num2;
	}

	@Override
	public double multiplicacion(double num1, double num2) {
		return num1 * num2;
	}

	@Override
	public double potencias(double num1, double num2) {
		return Math.pow((int)num1, (int)num2);
	}

	@Override
	public double raizCuadrada(double num1) {
		return Math.sqrt((int)num1);
	}

	@Override
	public double raizCubica(double num1) {
		return Math.pow(num1, 1.0/3.0);
	}

	@Override
	public double division(double num1, double num2) throws ArithmeticException {
		return  num1 / num2;
	}
	
}
