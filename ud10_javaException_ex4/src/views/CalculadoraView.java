package views;

import java.util.InputMismatchException;
import java.util.Scanner;

import dto.Operaciones;

public class CalculadoraView {

	private Scanner opcion = new Scanner(System.in); 

	private int primeroIntro = 0;
	private int segundoIntro = 0;
	
	public void menuCalculadora() {		
		try {
			System.out.println("********* Calculadora *********");
			System.out.println("(1) Sumar "
					+ "\n(2) Restar "
					+ "\n(3) Multiplicaci�n "
					+ "\n(4) Potencias "
					+ "\n(5) Ra�z cuadrada "
					+ "\n(6) Ra�z cubica "
					+ "\n(7) Divisi�n");
			System.out.print("Introduce una opci�n: ");
			int opcionIntro = opcion.nextInt();
			
			if(opcionIntro == 5 || opcionIntro == 6) {				
				System.out.print("\nIntroduce el primer n�mero de la operacion: ");
				 primeroIntro = opcion.nextInt();
			}else {
				System.out.print("\nIntroduce el primer n�mero de la operacion: ");
				 primeroIntro = opcion.nextInt();
				
				System.out.print("Introduce el segundo n�mero de la operacion: ");
				 segundoIntro = opcion.nextInt();

			}
			
			new CalculosSimples(opcionIntro, primeroIntro, segundoIntro);
			
			opcion.close();
		}catch (InputMismatchException e) {
			System.out.println("Solo puedes insertar n�meros. ");		
		}catch(ArithmeticException e){
			System.out.println("ERROR: "+e.getMessage());
        }catch (Exception e) {
			System.out.println(e.getMessage());
		}finally{
			System.out.println("\nFin Programa!!!");
        }
		
	}	
}
