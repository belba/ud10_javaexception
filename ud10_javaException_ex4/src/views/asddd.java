package views;

import java.util.InputMismatchException;
import java.util.Scanner;

import dto.Operaciones;

public class asddd {

	public void pepe() {
		Scanner opcion = new Scanner(System.in); 
		Operaciones operacion = new Operaciones();
		
		int primeroIntro = 0;
		int segundoIntro = 0;
		
		try {
			System.out.println("********* Calculadora *********");
			System.out.println("(1) Sumar "
					+ "\n(2) Restar "
					+ "\n(3) Multiplicaci�n "
					+ "\n(4) Potencias "
					+ "\n(5) Ra�z cuadrada "
					+ "\n(6) Ra�z cubica "
					+ "\n(7) Divisi�n");
			System.out.print("Introduce una opci�n: ");
			int opcionIntro = opcion.nextInt();
			
			if(opcionIntro == 5 || opcionIntro == 6) {				
				System.out.print("\nIntroduce el primer n�mero de la operacion: ");
				 primeroIntro = opcion.nextInt();
			}else {
				System.out.print("\nIntroduce el primer n�mero de la operacion: ");
				 primeroIntro = opcion.nextInt();
				
				System.out.print("Introduce el segundo n�mero de la operacion: ");
				 segundoIntro = opcion.nextInt();
	
			}

			switch (opcionIntro) {
			
				case 1:							
					System.out.print("Resultado: "+primeroIntro +" + "+ segundoIntro +" = "+operacion.suma(primeroIntro, segundoIntro));
					break;
				case 2:
					System.out.print("Resultado: "+primeroIntro +" - "+ segundoIntro +" = "+operacion.resta(primeroIntro, segundoIntro));
					break;
				case 3:
					System.out.print("Resultado: "+primeroIntro +" * "+ segundoIntro +" = "+operacion.multiplicacion(primeroIntro, segundoIntro));
					break;
				case 4:
					System.out.print("Resultado: "+primeroIntro +" elevado a la "+ segundoIntro +" = "+ operacion.potencias(primeroIntro, segundoIntro));
					break;
				case 5:
					System.out.print("Resultado: Ra�z cuadrada de "+primeroIntro +" = "+ operacion.raizCuadrada(primeroIntro));
					break;		
				case 6:
					System.out.print("Resultado: Ra�z cubica de "+primeroIntro +" = "+ operacion.raizCubica(opcionIntro));
					break;	
				case 7:
					System.out.print("Resultado: "+primeroIntro +" / "+ segundoIntro +" = "+operacion.division(primeroIntro, segundoIntro));
					break;	
				default:
					break;		
			
			};
		} catch (InputMismatchException e) {
			System.out.println("Solo puedes insertar n�meros. ");		
		}catch(ArithmeticException e){
			System.out.println("ERROR: "+e.getMessage());
        }catch (Exception e) {
			System.out.println(e.getMessage());
		}finally{
			System.out.println("\nFin Programa!!!");
        }
		
		opcion.close();
	}
}
