package views;

import dto.Operaciones;

public class CalculosSimples {
	
	private Operaciones operacion = new Operaciones();
	
	
	public CalculosSimples(int opcionIntro, int primeroIntro, int segundoIntro) {
		
		switch (opcionIntro) {		
			case 1:							
				System.out.print("Resultado: "+primeroIntro +" + "+ segundoIntro +" = "+operacion.suma(primeroIntro, segundoIntro));
				break;
			case 2:
				System.out.print("Resultado: "+primeroIntro +" - "+ segundoIntro +" = "+operacion.resta(primeroIntro, segundoIntro));
				break;
			case 3:
				System.out.print("Resultado: "+primeroIntro +" * "+ segundoIntro +" = "+operacion.multiplicacion(primeroIntro, segundoIntro));
				break;
			case 4:
				System.out.print("Resultado: "+primeroIntro +" elevado a la "+ segundoIntro +" = "+ operacion.potencias(primeroIntro, segundoIntro));
				break;
			case 5:
				System.out.print("Resultado: Ra�z cuadrada de "+primeroIntro +" = "+ operacion.raizCuadrada(primeroIntro));
				break;		
			case 6:
				System.out.print("Resultado: Ra�z cubica de "+primeroIntro +" = "+ operacion.raizCubica(opcionIntro));
				break;	
			case 7:
				System.out.print("Resultado: "+primeroIntro +" / "+ segundoIntro +" = "+operacion.division(primeroIntro, segundoIntro));
				break;	
			default:
				break;		
	
		};
	}

}
