package views;

import java.util.InputMismatchException;
import java.util.Scanner;

public class AdivinanzasView {
	
	private static final int NUMEROMAX = 500;
	
	private boolean salirBucle = true;
	private int count = 0;
	
	private int valoresGenerados = (int) Math.floor(Math.random()*NUMEROMAX+1);
	private Scanner numero = new Scanner(System.in); 	
	
	public void generarAdivinanza() {
				
		System.out.println("Bienvendio al juego de las adivinanzas");
		
		while(salirBucle) {			
			try {
				System.out.print("Introduce un n�mero entre 1 y "+NUMEROMAX+": ");
				
				int numeroIntro = numero.nextInt();

				if(numeroIntro == valoresGenerados) {
					System.out.println("Felicidades a adivinado el n�mero");
					System.out.println("N�mero de intentos: "+count);
					
					salirBucle = false;
				}else {
					count++;
				}
				if (salirBucle) {
					if(numeroIntro < valoresGenerados) {
						System.out.println("El n�mero es mayor");
							
					}else {
						System.out.println("El n�mero es menor");
						
					}
				}
				
			} catch (InputMismatchException e) {
				System.out.println("Solo puedes insertar n�meros. ");
				count++;
				numero.next();
			}
			
		}
		numero.close();
	}
}
