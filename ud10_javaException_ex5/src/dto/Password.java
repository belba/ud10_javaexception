package dto;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import sun.security.util.Length;

public class Password implements IPassword {
	
	private static final int maxLongitud = 8;
	
	//Atributos
	private int longitud;
	private String contrase�a;	
	
	
	//Constructores
	public Password() {
		this.longitud = maxLongitud;
		this.contrase�a = "";
	}

	public Password(int longitud, String contrase�a) {
		this.longitud = longitud;
		this.contrase�a = contrase�a;
	}


	/**
	 * @return the longitud
	 */
	public int getLongitud() {
		return longitud;
	}
	

	/**
	 * @return the contrase�a
	 */
	public String getContrase�a() {
		return contrase�a;
	}



	/**
	 * @param contrase�a the contrase�a to set
	 */
	public void setContrase�a(String contrase�a) {
		this.contrase�a = contrase�a;
	}



	@Override
	public boolean esFuerte(String contrase�a) {
		
		//Esta funcion esta sacada de la web (https://stackoverrun.com/es/q/6917213)
		int countMayuscula = (int) contrase�a.chars().filter((s)->Character.isUpperCase(s)).count();
		int countMenuscula = (int) contrase�a.chars().filter((s)->Character.isLowerCase(s)).count();
		int countNumeros =  (int) contrase�a.chars().filter((s)->Character.isDigit(s)).count();
		
		if(countMayuscula >= 2 && countMenuscula >= 1 && countNumeros >= 5) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public String generarPassword(int longitud) {
		
		// Genera una contrase�a a partir de la lengitud pasada por parametro
		// Funcion para generar contrase�a random sacada y adaptada de la web 
		// http://chuwiki.chuidiang.org/index.php?title=Generar_n%C3%BAmeros_aleatorios_en_Java
		
		char [] chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ".toCharArray();
		
		// Longitud del array de char.
		int charsLength = chars.length;

		// Instanciamos la clase Random
		Random random = new Random();

		// Un StringBuffer para componer la cadena aleatoria de forma eficiente
		StringBuffer buffer = new StringBuffer();

		// Bucle para elegir una cadena de 10 caracteres al azar
		for (int i=0;i<longitud;i++){
		   // A�adimos al buffer un caracter al azar del array
		   buffer.append(chars[random.nextInt(charsLength)]);
		}
		
		return buffer.toString();
		
	}


	
}
