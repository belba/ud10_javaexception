package dto;


public interface IPassword {
	
	public boolean esFuerte(String contraseņa); 
	
	public String generarPassword(int longitud);
}
