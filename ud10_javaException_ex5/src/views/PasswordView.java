package views;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

import dto.Password;

public class PasswordView {
	private Scanner contraseña = new Scanner(System.in); 	
	private Password p1 = new Password();
	private ArrayList<String> contraseñaGeneradas = new ArrayList<String>();
	private ArrayList<Boolean> contraseñaGeneradasEsFuerte = new ArrayList<Boolean>();
	
	public void generarPassword() {
		try {
			System.out.print("Introduce la cantidad de contraseñas que desea generar: ");
			int contraseñaCantidadIntro = contraseña.nextInt();

			System.out.print("Introduce longitud de la contraseñas que desea generar: ");
			int contraseñaLongitudIntro = contraseña.nextInt();
			
			// Generar contraseña y almacenarla en array list
			for (int i = 0; i < contraseñaCantidadIntro; i++) {		
				contraseñaGeneradas.add(p1.generarPassword(contraseñaLongitudIntro));
			}
			
			// Almacenar en array list si la contraseña es fuerte o no
			for (int i = 0; i < contraseñaGeneradas.size(); i++) {
				contraseñaGeneradasEsFuerte.add(p1.esFuerte(contraseñaGeneradas.get(i)));
			    }
			
			for (int i = 0; i < contraseñaCantidadIntro; i++) {
				
				System.out.println("La contraseña: "+contraseñaGeneradas.get(i).toString() +" Es fuerte: "+contraseñaGeneradasEsFuerte.get(i).booleanValue());
			}
			
			contraseña.close();
		}catch (ArrayStoreException e) {
			
		} catch (ArrayIndexOutOfBoundsException e){
			System.out.println(e.getMessage());
		} catch (InputMismatchException e) {
			System.out.println("Solo puedes insertar números. ");		
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}finally{
			System.out.println("\nFin Programa!!!");
        }
		
	}
}
